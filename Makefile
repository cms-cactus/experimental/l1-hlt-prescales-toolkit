SHELL:=/bin/bash
.DEFAULT_GOAL := pstools

#######
# hard to remember golang vendoring commands
#######
.PHONY: dependencies
dependencies: pstools_cli pstools_scriptrunner pstools_api
	# Update all direct and indirect dependencies to latest minor or patch upgrades
	go get -u ./...
	# Prune any no-longer-needed dependencies from go.mod and add any dependencies needed for other combinations of OS, architecture, and build tags
	go mod tidy
	# create vendor directory for offline building
	# go build does NOT use the vendor directory by default, this directory a fail-safe for takedowns of dependencies
	go mod vendor

.PHONY: test
test:
	echo todo

.PHONY: rpm
rpm:
	echo todo

#######
# sub-projects in /external
#######
# all external dirs that have a make file
external_dirs := $(shell find external -type f -name Makefile | xargs -n1 dirname | grep -ve /vendor -e /dist)

.PHONY: external $(external_dirs)
external: $(external_dirs)
$(external_dirs):
	$(MAKE) -C $@


#######
# web interface
#######
webui_folder := web/ui
webui_src_files := $(shell find ${webui_folder} -type f | grep -v -e 'node_modules' | sed 's|\s|\\ |g')

${webui_folder}/node_modules:
	# --unsafe-perm to make it work in docker containers
	cd ${webui_folder} && npm i --unsafe-perm

${webui_folder}/build: ${webui_folder}/node_modules $(webui_src_files)
	cd ${webui_folder} && npx vue-cli-service lint --max-warnings 0 && npx vue-cli-service build --mode production

${webui_folder}/production.go: ${webui_folder}/build
	go generate ./internal/generators/web/generator.go

.PHONY: webui
webui: ${webui_folder}/production.go

#######
# pstools
#######
pstools_external_configs := $(shell find external -type f -name cli.yaml | grep -v _examples)

pstools_cli_generated := $(shell find external -type f -name cli.yaml | grep -v _examples | sed -E 's|.*/([^/]+)/cli.yaml|\1|' | awk '$$0="internal/cli/generated_"$$0".go"')
$(pstools_cli_generated): $(pstools_external_configs)
	cd ./internal/generators/cli && go run ./generator.go $(shell echo $@ | sed -E 's|.*generated_(.*).go|\1|')
.PHONY: pstools_cli
pstools_cli: $(pstools_cli_generated)

pstools_scriptrunner_generated := $(shell find external -type f -name cli.yaml | grep -v _examples | sed -E 's|.*/([^/]+)/cli.yaml|\1|' | awk '$$0="internal/scriptrunner/external/generated_"$$0".go"')
$(pstools_scriptrunner_generated): $(pstools_external_configs)
	mkdir -p internal/scriptrunner/external
	cd ./internal/generators/scriptrunner && go run ./generator.go $(shell echo $@ | sed -E 's|.*generated_(.*).go|\1|')
.PHONY: pstools_scriptrunner
pstools_scriptrunner: $(pstools_scriptrunner_generated)

pstools_api_generated := $(shell find external -type f -name cli.yaml | grep -v _examples | sed -E 's|.*/([^/]+)/cli.yaml|\1|' | awk '$$0="internal/web/api/v1/generated_"$$0".go"')
$(pstools_api_generated): $(pstools_external_configs)
	cd ./internal/generators/api && go run ./generator.go $(shell echo $@ | sed -E 's|.*generated_(.*).go|\1|')
.PHONY: pstools_api
pstools_api: $(pstools_api_generated)

pstools_src_files := $(shell find internal web  cmd -type f | grep -v -e 'production.go' -e 'web/ui' | sed 's|\s|\\ |g')
pstools: ${pstools_src_files} webui external pstools_cli pstools_scriptrunner pstools_api dependencies
	go build -o build/pstools -tags=production ./cmd/pstools

#######
# clean
#######
.PHONY: clean
clean:
	git clean -dfX
