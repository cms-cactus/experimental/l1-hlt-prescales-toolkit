#!/usr/bin/env python3

import os
import sys
import time
import json
import xml.etree.ElementTree as ET
from xml.dom import minidom
from datetime import datetime, timezone

try:
  os.stat(3)
  os.write(2, b'running under pstools\n')
  def write(s):
    return os.write(3, s)
except:
  os.write(2, b'running standalone\n')
  def write(s):
    return 0

if len(sys.argv) < 1:
  raise Exception('not enough arguments')

outFileName = sys.argv[1]

basePath = os.getenv('PSTOOLS_PRESCALES_REPO_LOCAL')
if basePath is None:
  os.write(2, b'PSTOOLS_PRESCALES_REPO_LOCAL env variable not set\n')

with open(os.path.join(basePath, 'l1.json')) as f:
  l1Prescales = json.load(f)

with open(os.path.join(basePath, 'columns.json')) as f:
  columnsData = json.load(f)

outFilePath = os.path.join(basePath, outFileName)

os.write(1, bytes("writing " + outFilePath, 'utf-8'))

pstoolsversion = "(unknown version)"
xmlstr = ET.tostring(ET.Comment('PSTools ' + pstoolsversion)).decode()
xmlstr += ET.tostring(ET.Comment('Generated on ' + datetime.now(timezone.utc).strftime("%a %b %d %Y %H:%M:%S %Z"))).decode()

parent = l1Prescales.get('prescales').get('parent')
if parent:
  xmlstr += ET.tostring(ET.Comment('from ' + parent)).decode()

root = ET.Element('run-settings')
root.set('id', 'uGT')
context = ET.SubElement(root, 'context')
context.set('id', 'uGtProcessor')
indexParam = ET.SubElement(context, 'param')
indexParam.text = 0
indexParam.set('id', 'index')
indexParam.set('type', 'uint')

prescalesParam = ET.SubElement(context, 'param')
prescalesParam.set('id', 'prescales')
prescalesParam.set('type', 'table')

columns = ET.SubElement(prescalesParam, 'columns')
columnsHeader = ["algo/prescale-index"]
for i, col in enumerate(columnsData.get('columns')):
  columnsHeader.append(str(i) + ": " + col.get('name'))
columns.text = ", ".join(columnsHeader)

prescaleTypes = ET.SubElement(prescalesParam, 'types')
prescaleTypes.text = ', '.join(['string'] + ['double'] * len(columnsData.get('columns')))

prescaleRows = ET.SubElement(prescalesParam, 'rows')
algos = l1Prescales.get('prescales').get('algorithms')
for name in algos:
  row = ET.SubElement(prescaleRows, 'row')
  prescales = algos.get(name).get('columns')
  row.text = name + ", " + ", ".join(map(str, prescales))

xmlstr += ET.tostring(root, encoding='utf8', method='xml', xml_declaration=False).decode()
xmlstr = minidom.parseString(xmlstr).toprettyxml(indent="  ", encoding="UTF-8").decode()
myfile = open(outFilePath, "w")
myfile.write(xmlstr)
