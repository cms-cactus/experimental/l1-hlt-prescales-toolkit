#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

environment=${1:-}
fd3on=false
if { >&3; } 2<> /dev/null; then
  fd3on=true
fi

writefd3 () {
  if $fd3on; then
   >&3 echo $@
  fi
}

echo "full arguments: $@"

if [[ -z "$environment" ]]; then
  >&2 echo "no environment set"
  exit 1
fi

echo "deploying to $environment"

echo "sending prescales to L1CE"
writefd3 '{"status": "sending prescales to L1CE", "environment": "' + $environment + '"}'

sleep 2s

echo "new keys created"
echo "  key1"
echo "  -> key2"
echo "     -> key3"
writefd3 '{"status": "new keys created", "keys": ["key1", "key2", "key3"]}'

# if $fd3on; then
#   echo "fd3 on"
#   >&3 echo '{"msg": "fd3 on"}'
# else
#   echo "fd3 off"
# fi