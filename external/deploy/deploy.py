#!/usr/bin/env python3

import sys
import os
import time

os.write(1, b'I am deploy.py\n')
os.write(1, bytes('My arguments are: ' + str(sys.argv), 'utf-8'))

try:
  os.stat(3)
  os.write(2, b'running under pstools\n')
  def write(s):
    return os.write(3, s)
except:
  os.write(2, b'running standalone\n')
  def write(s):
    return 0

os.write(1, b'result1: value1\n')
write(b'{"result1": "value1"}\n')

os.write(1, b'sleeping for 1s\n')
time.sleep(1)

os.write(1, b'result2: value2\n')
write(b'{"result2": "value2"}\n')