module pstools-vet-jsonschema

go 1.14

require (
	github.com/dustin/gojson v0.0.0-20160307161227-2e71ec9dd5ad
	github.com/sirupsen/logrus v1.6.0
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb // indirect
	github.com/xeipuuv/gojsonschema v1.2.0
	golang.org/x/sys v0.0.0-20200602225109-6fdc65e7d980 // indirect
)
