package jsonstepper

import (
	"fmt"
	"strconv"

	"github.com/dustin/gojson"
)

func escape(s string, out []rune) []rune {
	for _, c := range s {
		switch c {
		case '/':
			out = append(out, '~', '1')
		case '~':
			out = append(out, '~', '0')
		default:
			out = append(out, c)
		}
	}
	return out
}

func encodePointer(p []string) string {
	out := make([]rune, 0, 64)

	for _, s := range p {
		out = append(out, '/')
		out = escape(s, out)
	}
	return string(out)
}

func grokLiteral(b []byte) string {
	s, ok := json.UnquoteBytes(b)
	if !ok {
		panic("could not grok literal " + string(b))
	}
	return string(s)
}

func sliceToEnd(s []string) []string {
	end := len(s) - 1
	if end >= 0 {
		s = s[:end]
	}
	return s
}

func mustParseInt(s string) int {
	n, err := strconv.Atoi(s)
	if err == nil {
		return n
	}
	panic(err)
}

// PosFromPointer gives a line number and col number for the given pointer
func PosFromPointer(data []byte, wantedPointer string) (int, int, error) {
	if len(data) == 0 {
		return 0, 0, fmt.Errorf("Invalid JSON")
	}
	scan := &json.Scanner{}
	scan.Reset()

	line := 1
	col := 1

	offset := 0
	beganLiteral := 0
	var current []string
	for {
		if offset >= len(data) {
			return 0, 0, nil
		}

		if data[offset] == '\n' {
			col = 1
			line++
		} else {
			col++
		}

		newOp := scan.Step(scan, int(data[offset]))
		offset++

		switch newOp {
		case json.ScanBeginArray:
			current = append(current, "0")
		case json.ScanObjectKey:
			current[len(current)-1] = grokLiteral(data[beganLiteral-1 : offset-1])
		case json.ScanBeginLiteral:
			beganLiteral = offset
		case json.ScanArrayValue:
			n := mustParseInt(current[len(current)-1])
			current[len(current)-1] = strconv.Itoa(n + 1)
		case json.ScanEndArray, json.ScanEndObject:
			current = sliceToEnd(current)
		case json.ScanBeginObject:
			current = append(current, "")
		case json.ScanError:
			return 0, 0, fmt.Errorf("Error reading JSON object at offset %v", offset)
		}

		if newOp == json.ScanBeginArray || newOp == json.ScanArrayValue ||
			newOp == json.ScanObjectKey {
			pointer := encodePointer(current)
			if pointer == wantedPointer {
				return line, col, nil
			}
		}
	}
}
