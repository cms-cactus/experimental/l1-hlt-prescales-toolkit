package internal

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"pstools-vet-jsonschema/internal/jsonstepper"

	"github.com/xeipuuv/gojsonschema"
)

type fd3Type string

const (
	fd3ErrorType fd3Type = "error"
)

type fd3Response struct {
	ResponseType fd3Type `json:"type"`
	File         string  `json:"filename"`
	Data         interface{}
}

type fd3ErrorData struct {
	Message string `json:"message"`
	Line    int    `json:"line"`
	Col     int    `json:"col"`
}

func posFromJSONPointer(filePath string, pointer string) (int, int, error) {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return 0, 0, err
	}
	line, col, err := jsonstepper.PosFromPointer(data, pointer)
	if err != nil {
		return 0, 0, err
	}
	return line, col, nil
}

func posFromOffset(filePath string, offset int64) (int, int, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return 0, 0, err
	}
	defer f.Close()
	buffer := make([]byte, offset)
	bytesRead, err := f.Read(buffer)
	if err != nil {
		return 0, 0, err
	}
	if int64(bytesRead) != offset {
		return 0, 0, errors.New("can't read file")
	}
	line := 1
	pos := 1
	for _, b := range buffer {
		if b == '\n' {
			line++
			pos = 1
		} else {
			pos++
		}
	}
	return line, pos, nil
}

func syntaxErrToFd3(syntaxErr *json.SyntaxError, filepath string, filename string) []byte {
	var data fd3ErrorData
	resp := fd3Response{fd3ErrorType, filename, data}
	data.Message = syntaxErr.Error()
	line, pos, err := posFromOffset(filepath, syntaxErr.Offset)
	if err != nil {
		data.Message = err.Error()
	} else {
		data.Line = line
		data.Col = pos
	}

	resp.Data = data
	bytes, _ := json.Marshal(resp)
	writeFd3(bytes)

	return bytes
}

func resultErrToFd3(structError gojsonschema.ResultError, filepath string, filename string) []byte {
	shittyPointer := structError.Field()
	pointer := "/" + strings.ReplaceAll(shittyPointer, ".", "/")

	var data fd3ErrorData
	data.Message = structError.Description()

	data.Line, data.Col, _ = posFromJSONPointer(filepath, pointer)

	resp := fd3Response{fd3ErrorType, filename, data}
	bytes, _ := json.Marshal(resp)
	writeFd3(bytes)

	return bytes
}

func errMsgToFd3(msg string, filepath string, filename string) []byte {
	var data fd3ErrorData
	data.Message = msg
	resp := fd3Response{fd3ErrorType, filename, data}
	bytes, _ := json.Marshal(resp)
	writeFd3(bytes)
	return bytes
}

func handleJSONErr(err interface{}, filepath string, filename string) {
	if err != nil {
		l := log.WithField("error", err)
		var b []byte
		if err, ok := err.(*json.SyntaxError); ok {
			b = syntaxErrToFd3(err, filepath, filename)
			l = l.WithField("fd3", string(b))
		}
		if err, ok := err.(gojsonschema.ResultError); ok {
			b = resultErrToFd3(err, filepath, filename)
			l = l.WithField("fd3", string(b))
		}
		if b == nil {
			if err, ok := err.(error); ok {
				b = errMsgToFd3(err.Error(), filepath, filename)
				l = l.WithField("fd3", string(b))
			}
			log.WithField("error", err).WithField("type", fmt.Sprintf("%T", err)).Error("unknown json error")
		}
		l.Error("validation failed")
	}
}

// Validate runs validation on all json documents
func Validate() {
	basePath := os.Getenv("PSTOOLS_PRESCALES_REPO_LOCAL")
	if basePath == "" {
		log.Fatal("Prescales base folder not given. Env variable PSTOOLS_PRESCALES_REPO_LOCAL unset")
	}

	filenames := []string{"columns", "l1", "hlt"}
	for _, n := range filenames {
		validate(basePath, n+".json", n+".schema.json")
	}
	log.Info("all validations passed")
}

func validate(folder string, jsonFilename string, schemaFilename string) {
	jsonPath := path.Join(folder, jsonFilename)
	schemaPath := path.Join(folder, schemaFilename)
	log := log.WithField("jsonPath", jsonPath).WithField("schemaPath", schemaPath)

	log.Info("starting validation")
	schemaLoader := gojsonschema.NewReferenceLoader("file://" + schemaPath)
	documentLoader := gojsonschema.NewReferenceLoader("file://" + jsonPath)

	schema, err := gojsonschema.NewSchema(schemaLoader)
	handleJSONErr(err, schemaPath, schemaFilename)
	if schema == nil {
		log.Fatal("schema validition failed")
	}
	result, err := schema.Validate(documentLoader)
	handleJSONErr(err, jsonPath, jsonFilename)

	if !result.Valid() {
		for _, err := range result.Errors() {
			handleJSONErr(err, jsonPath, jsonFilename)
		}
		log.Fatal("validition failed")
	}
	log.Info("validation passed")
}
