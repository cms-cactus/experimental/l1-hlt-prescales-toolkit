package main

import (
	"os"
	"pstools-examples-helloworld/internal"
)

func main() {
	name := ""
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	internal.HelloWorld(name)
}
