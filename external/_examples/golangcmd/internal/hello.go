package internal

import (
	"encoding/json"
	"fmt"
)

type fd3Message struct {
	Message string
}

// HelloWorld prints a hello message for the specified name, defaults to 'World'
func HelloWorld(helloWho string) {
	if helloWho == "" {
		helloWho = "World"
	}
	msg := fmt.Sprintf("Hello, %s!", helloWho)
	if fd3 == nil {
		fmt.Println(msg)
	} else {
		bytes, _ := json.Marshal(fd3Message{msg})
		writeFd3(bytes)
	}
}
