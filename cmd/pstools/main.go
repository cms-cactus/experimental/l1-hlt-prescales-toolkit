package main

import (
	"l1-hlt-prescales-toolkit/internal/config"
	"l1-hlt-prescales-toolkit/internal/git"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

func main() {
	InitCli()
}

var rootCmd = &cobra.Command{
	Use:   "L1+HLT PS Tools",
	Short: "Level-1 & HLT Prescales toolkit",
}

func init() {
	cobra.OnInitialize(config.CobraInit(&cfgFile), func() {
		if err := git.VerifyLocal(); err != nil {
			log.WithError(err).Fatal("cannot verify prescales git repo")
			panic(err)
		}
	})

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config.file", "", "config file")

	rootCmd.PersistentFlags().String(config.KeyLoglevel.Name, "", config.KeyLoglevel.Description)
	viper.BindPFlag(config.KeyLoglevel.Name, rootCmd.PersistentFlags().Lookup(config.KeyLoglevel.Name))
}

// InitCli adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func InitCli() {
	err := rootCmd.Execute()
	if err != nil {
		log.WithError(err).Fatal("cannot start CLI")
	}
}
