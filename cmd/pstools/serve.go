package main

import (
	"l1-hlt-prescales-toolkit/internal/config"
	"l1-hlt-prescales-toolkit/web/api"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var serveCmd = &cobra.Command{
	Use:     "serve",
	Aliases: []string{"ui", "gui", "server"},
	Short:   "Start a webserver with the prescale editor",
	Run: func(cmd *cobra.Command, args []string) {
		err := api.StartAndListen(*port)
		if err != nil {
			log.WithError(err).Fatal("cannot start api")
		}
	},
}

var port *string

func init() {
	rootCmd.AddCommand(serveCmd)
	port = serveCmd.Flags().StringP("port", "p", "", config.KeyAPIPort.Description)
	viper.BindPFlag(config.KeyAPIPort.Name, serveCmd.Flags().Lookup("port"))
}
