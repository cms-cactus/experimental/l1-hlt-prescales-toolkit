ARG BASE_OS=cc7
ARG BASE_TAG
FROM cern/${BASE_OS}-base:${BASE_TAG}
LABEL maintainer="Glenn Dirkx <glenn.dirkx@cern.ch>"

ENV GO_VERSION=1.14
ENV NODE_VERSION=12.16.1
ENV PATH="$PATH:/usr/local/go/bin:/usr/local/lib/nodejs/node-v${NODE_VERSION}-linux-x64/bin"

COPY kubernetes.repo /etc/yum.repos.d/kubernetes.repo

# yum packages
RUN yum install -y make xz-utils gcc git libxml2-devel gcc-c++ python3 kubectl

# golang
RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz

# nodejs
RUN curl -o node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz && \
    mkdir -p /usr/local/lib/nodejs && tar -xJvf node.tar.xz -C /usr/local/lib/nodejs && rm node.tar.xz

# python stuff
RUN pip3 install pipenv pyinstaller