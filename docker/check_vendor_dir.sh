#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`/..

make dependencies
STATUS=$(git status vendor go.mod go.sum --porcelain --ignore-submodules=dirty | tail -n1)
if [[ -n $STATUS ]]; then
  >&2 echo "vendoring info changed after running 'make dependencies'"
  >&2 echo "please run 'make dependencies' and commit changes"
  exit 1
else
  echo "vendor directory is up-to-date"
fi