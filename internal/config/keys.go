package config

import "github.com/spf13/viper"

// Key describes a config key
type Key struct {
	Name        string
	Description string
}

func k(name string, description string, defaultValue interface{}) *Key {
	viper.SetDefault(name, defaultValue)
	return &Key{name, description}
}

// list of config keys to be used with viper.Get(<name>)
var (
	KeyLoglevel  = k("log.level", "log level", "info")
	KeyGitRemote = k("prescales.repo.remote", "remote repository holding prescale data", "https://gitlab.cern.ch/cms-cactus/experimental/l1-hlt-prescales.git")
	KeyGitLocal  = k("prescales.repo.local", "local directory to clone the prescales repo into", "l1-hlt-prescales")
	KeyAPIPort   = k("api.port", "API port to listen to", 0)
)
