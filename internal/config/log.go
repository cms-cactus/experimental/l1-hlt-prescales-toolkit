package config

import (
	l "github.com/sirupsen/logrus"
)

var log = l.WithField("package", "config")
