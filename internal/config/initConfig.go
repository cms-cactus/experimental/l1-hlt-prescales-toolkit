package config

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var cfgFileOverride *string

const envPrefix = "PSTOOLS"

var envTransformer = strings.NewReplacer(".", "_")

// CobraInit for use with Cobra
func CobraInit(p *string) func() {
	cfgFileOverride = p
	return InitConfig
}

// InitConfig parses the config
func InitConfig() {
	configFolder := os.Getenv("XDG_CONFIG_HOME")
	if configFolder == "" {
		home := os.Getenv("HOME")
		if home != "" && !strings.HasSuffix(home, "/") {
			home = home + "/"
		}
		configFolder = home + ".config/pstools/"
	}

	if cfgFileOverride != nil && *cfgFileOverride != "" {
		viper.SetConfigFile(*cfgFileOverride)
	} else {
		viper.AddConfigPath(configFolder)
		// read https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf
		// page 9 specifies why to prefer /etc/opt
		viper.AddConfigPath("/etc/opt/cactus/pstools/config/")
		viper.AddConfigPath(".")
		viper.SetConfigName("config")
	}

	// read in env variables, env variables override config file
	// example: 'loglevel: info' in config.yaml will be overriden by command
	//          'PSTOOLS_LOGLEVEL=ERROR go run main.go'
	viper.SetEnvPrefix(envPrefix)
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(envTransformer)

	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.WithError(err).WithField("file", *cfgFileOverride).Fatal("specified config file not found")
		} else {
			log.WithError(err).WithField("file", viper.ConfigFileUsed()).Fatal("an error occurred while reading config file")
		}
	}
	log.WithField("file", viper.ConfigFileUsed()).Info("config file found")

	loglevel := viper.GetString(KeyLoglevel.Name)
	if loglevel != "" {
		log.WithField("level", loglevel).Info("setting log level")
	}

	var level logrus.Level = logrus.InfoLevel
	switch loglevel {
	case "trace":
		level = logrus.TraceLevel
	case "debug":
		level = logrus.DebugLevel
	case "info":
		level = logrus.InfoLevel
	case "warn", "warning":
		level = logrus.WarnLevel
	case "error":
		level = logrus.ErrorLevel
	case "fatal":
		level = logrus.FatalLevel
	default:
		log.WithField("level", loglevel).Error("log level specified in config does not exist")
	}

	logrus.SetLevel(level)

	// set environment variables from config
	// this way, programs in /external can read these too and use config variables
	for _, key := range []*Key{KeyLoglevel, KeyGitLocal} {
		env := envPrefix + "_" + strings.ToUpper(envTransformer.Replace(key.Name))
		val := viper.GetString(key.Name)
		if key == KeyGitLocal {
			val, _ = filepath.Abs(val)
		}
		log.WithField("env", env).WithField("key", key.Name).WithField("val", val).Debug("setting env key")
		os.Setenv(env, val)
	}
}
