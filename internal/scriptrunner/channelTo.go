package scriptrunner

import (
	"io"
	"net/http"
	"os"
	"sync"
)

type flushWriter struct {
	f http.Flusher
	w io.Writer
}

// Drain takes all channel content and throws it away
func Drain(ch chan string) {
	for range ch {

	}
}

// ChannelTo prints all lines to the given file descriptor, with optional prefix
func ChannelTo(wg *sync.WaitGroup, w io.Writer, prefix string, lines chan string) {
	defer wg.Done()

	// if our writer supports intermediate flushing of data, use it.
	// i.e. long running jobs will receive data from the writer as it is written
	var flusher http.Flusher
	if f, ok := w.(http.Flusher); ok {
		flusher = f
	}
	for line := range lines {
		if prefix != "" {
			line = prefix + ": " + line
		}
		line += "\n"
		w.Write([]byte(line))
		if flusher != nil {
			flusher.Flush()
		}
	}
}

// ChannelToStdout is a shorthand for ChannelTo(os.Stdout, ...)
func ChannelToStdout(wg *sync.WaitGroup, prefix string, lines chan string) {
	ChannelTo(wg, os.Stdout, prefix, lines)
}

// ChannelToStderr is a shorthand for ChannelTo(os.Stdout, ...)
func ChannelToStderr(wg *sync.WaitGroup, prefix string, lines chan string) {
	ChannelTo(wg, os.Stderr, prefix, lines)
}

// ChannelToChannel drains the given channel into the other given channel
func ChannelToChannel(wg *sync.WaitGroup, a chan string, b chan string, name string) {
	defer wg.Done()
	for s := range a {
		b <- s
	}
}
