package scriptrunner

import (
	"os"
	"path"
	"path/filepath"
)

// Externaldir holds the location of the 'external' folder
var externaldir string

func init() {
	binaryDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic("can't get pstools directory")
	}
	cwd, err := os.Getwd()
	if err != nil {
		panic("can't get working directory")
	}
	for _, basePath := range []string{binaryDir, "/opt/cactus/pstools", cwd} {
		for _, d := range []string{"external", "../external"} {
			p := path.Join(basePath, d)
			// log.WithField("path", p).Info("looking for 'external' directory")
			if s, err := os.Stat(p); err == nil && s.IsDir() {
				externaldir = p
				break
			}
		}
	}
	if externaldir == "" {
		log.Fatal("cannot find 'external' directory")
	}
	log.WithField("dir", externaldir).Info("external directory found")
}

// GetPackageDir returns the base path where one can run an external package with the given name
func GetPackageDir(name string) string {
	return path.Join(externaldir, name)
}
