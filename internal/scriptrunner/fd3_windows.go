package scriptrunner

import (
	"errors"
	"net"
	"os/exec"
	"time"

	winio "github.com/Microsoft/go-winio"
)

const pipeName = "\\\\.\\pipe\\pstoolspipe"

func attachFd3(cmd *exec.Cmd, lines chan string, done chan error) (closeable, error) {
	l, err := winio.ListenPipe(pipeName, nil)
	if err != nil {
		return nil, err
	}

	connected := make(chan net.Conn, 1)
	notConnected := make(chan error, 1)
	go func() {
		defer close(connected)
		defer close(notConnected)
		conn, err := l.Accept()
		if err != nil {
			notConnected <- err
		} else {
			connected <- conn
		}
	}()
	go func() {
		select {
		case <-time.After(5 * time.Second):
			done <- errors.New("pipe connect timeout")
		case err := <-notConnected:
			done <- err
		case reader := <-connected:
			readerToChannel(reader, lines, done)
		}
	}()
	return l, nil
}
