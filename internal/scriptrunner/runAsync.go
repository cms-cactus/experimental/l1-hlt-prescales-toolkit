package scriptrunner

import (
	"errors"
	"os"
	"os/exec"
	"time"
)

// RunAsync executes a given command for you and supplies a channel
// for stdout, stderr, and fd3
// fd3 is implemented platform-specific and is intended as the proper source to get script output
// this function does not wait for the specified command to finish, use the done channel for that
func (job *Job) RunAsync() (
	stdout chan string,
	stderr chan string,
	fd3 chan string,
	done chan error,
) {
	stdout = make(chan string)
	stderr = make(chan string)
	fd3 = make(chan string)
	done = make(chan error, 1)

	cmd := exec.Command(job.ScriptPath, job.Arg...)
	cmd.Dir = job.Dir

	/*
	 * agnostic code, linux and win have stdout and stderr
	 */
	stdoutr, stdoutw, err := os.Pipe()
	if err != nil {
		done <- err
		return
	}
	stderrr, stderrw, err := os.Pipe()
	if err != nil {
		done <- err
		return
	}
	cmd.Stdout, cmd.Stderr = stdoutw, stderrw
	doneReading := make(chan error)
	go readerToChannel(stdoutr, stdout, doneReading)
	go readerToChannel(stderrr, stderr, doneReading)

	/*
	 * platform specific, linux uses fd3, windows uses named pipes
	 */
	fd3cancel, err := attachFd3(cmd, fd3, doneReading)
	if err != nil {
		done <- err
		return
	}

	err = cmd.Start()

	doneScripting := make(chan error)
	go func() {
		doneScripting <- cmd.Wait()
		close(doneScripting)
	}()

	timeout := time.After(job.Timeout)

	go func() {
		select {
		case <-timeout:
			cmd.Process.Kill()
			stdoutw.Close()
			stderrw.Close()
			fd3cancel.Close()
			done <- errors.New("script timed out")
		case err := <-doneScripting:
			stdoutw.Close()
			stderrw.Close()
			fd3cancel.Close()
			if err != nil {
				done <- err
			}

			for i := 0; i < 3; i++ { // 3 = stdout + stderr + fd3
				if err := <-doneReading; err != nil {
					done <- err
				}
			}
			done <- nil
		}
		close(doneReading)
		// close(stdout)
		// close(stderr)
		// close(fd3)
		close(done)
	}()

	return
}
