package external

// Export does nothing
func Export() (
	stdout chan string,
	stderr chan string,
	fd3 chan string,
	done chan error,
) {
	return emptyCmd("please specify what exporter to run")
}
