package external

import "errors"

func emptyCmd(errormsg string) (chan string, chan string, chan string, chan error) {
	stdout := make(chan string, 0)
	stderr := make(chan string, 0)
	fd3 := make(chan string, 0)
	done := make(chan error, 1)

	close(stdout)
	close(stderr)
	close(fd3)

	if errormsg != "" {
		done <- errors.New(errormsg)
	} else {
		done <- nil
	}
	return stdout, stderr, fd3, done
}
