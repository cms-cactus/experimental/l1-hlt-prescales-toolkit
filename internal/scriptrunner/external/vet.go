package external

import (
	"l1-hlt-prescales-toolkit/internal/scriptrunner"
	"sync"
)

// Vet perform JSON Schema validations
func Vet() (
	stdout chan string,
	stderr chan string,
	fd3 chan string,
	done chan error,
) {
	stdout = make(chan string)
	stderr = make(chan string)
	fd3 = make(chan string)
	done = make(chan error, 1)

	todo := []func() (chan string, chan string, chan string, chan error){Vet_json}
	var firstErr error

	var loopwg sync.WaitGroup
	loopwg.Add(len(todo))

	for _, f := range todo {
		go func() {
			subStdout, subStderr, subFd3, subdone := f()
			var wg sync.WaitGroup
			wg.Add(3)
			go scriptrunner.ChannelToChannel(&wg, subStdout, stdout, "stdout")
			go scriptrunner.ChannelToChannel(&wg, subStderr, stderr, "stderr")
			go scriptrunner.ChannelToChannel(&wg, subFd3, fd3, "fd3")
			wg.Wait()
			suberr := <-subdone
			loopwg.Done()
			if suberr != nil {
				firstErr = suberr
			}
		}()
	}

	go func() {
		loopwg.Wait()
		close(stdout)
		close(stderr)
		close(fd3)
		done <- firstErr
	}()

	return
}
