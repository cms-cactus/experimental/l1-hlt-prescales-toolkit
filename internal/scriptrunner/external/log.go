package external

import (
	l "github.com/sirupsen/logrus"
)

var log = l.WithField("package", "external")
