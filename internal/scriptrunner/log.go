package scriptrunner

import (
	l "github.com/sirupsen/logrus"
)

var log = l.WithField("package", "scriptrunner")
