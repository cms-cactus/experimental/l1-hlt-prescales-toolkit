//go:generate go run generator.go
// +build ignore

package main

import (
	apigenerator "l1-hlt-prescales-toolkit/internal/generators/api"
	"l1-hlt-prescales-toolkit/internal/generators/util"
	"os"
)

func main() {
	if len(os.Args) > 1 && os.Args[1] != "" {
		apigenerator.GenerateAPI(util.ExternalPackage(os.Args[1]))
	} else {
		apigenerator.GenerateAll()
	}
}
