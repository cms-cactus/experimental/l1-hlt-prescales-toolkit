package util

import "strings"

// TemplateStringArray takes an array of strings and
// returns '[]string{"items", ...}'
func TemplateStringArray(input []string) string {
	b := make([]string, len(input))
	for i, s := range input {
		b[i] = "\"" + s + "\""
	}
	return "[]string{" + strings.Join(b, ", ") + "}"
}
