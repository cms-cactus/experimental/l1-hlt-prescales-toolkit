package util

import "strings"

// NoSpaces takes a string and removes all whitespaces
func NoSpaces(run string) string {
	return strings.ReplaceAll(run, " ", "_")
}
