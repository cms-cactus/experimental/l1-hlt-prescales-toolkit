package util

import (
	"strings"
)

// Capitalize returns the given string with the first character in uppercase, rest in lowercase
func Capitalize(input string) string {
	return strings.ToUpper(string(input[0])) + strings.ToLower(string(input[1:]))
}
