package util

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// ExternalPackage is the name of a package in /external
type ExternalPackage string

// ExternalConfig is the root YAML config type that reads a cli.yaml from external packages
type ExternalConfig struct {
	// the base command to run, without arguments or flags
	Run string
	// set to true if you want to manually code the external.() function
	Stub bool
	// the command to register in the CLI
	Command string
	// <optional> an array of aliases that can be used instead of the first word in .use
	Aliases []string `yaml:",omitempty"`
	// <optional> defines if this command is deprecated and should print this string when used
	Deprecated string `yaml:",omitempty"`
	// <optional> defines if this command is hidden and should NOT show up in the list of available commands
	Hidden bool `yaml:",omitempty"`
	// the short description shown in the 'help' output
	Short string
	// <optional> the long message shown in the 'help <this-command>' output
	Long string `yaml:",omitempty"`
	// <optional> a string containing one or more example usages
	Example string `yaml:",omitempty"`
	// <optional> list of arguments
	Args []ExternalConfigArg `yaml:",omitempty"`
	// <optional> list of flags
	Flags []ExternalConfigFlag `yaml:",omitempty"`
}

// ExternalConfigArg is part of ExternalConfig
type ExternalConfigArg struct {
	// argument name shown in the 'help' output
	Name string
	// <optional> wether argument must be given or not, and should print this string when absent
	Required string `yaml:",omitempty"`
	// <optional> default value
	DefaultValue string `yaml:"default,omitempty"`
	// <optional> regex to validate the argument value with
	Regex string `yaml:",omitempty"`
	// <optional> error message to print when the argument is invalid
	Errormsg string `yaml:",omitempty"`
}

// ExternalConfigFlag is part of ExternalConfig
type ExternalConfigFlag struct {
	// flag name shown in the 'help' output
	// note that this value is required, making it impossible to have short-only options like `ls -l`
	// you can thank an idiot called Nuru for that
	// https://github.com/spf13/cobra/issues/679#issuecomment-572923671
	// https://github.com/spf13/pflag/pull/171
	Name string
	// a short description what the flag does
	Description string `yaml:",omitempty"`
	// <optional=string> data type of the flag value
	ValueType string `yaml:"type,omitempty"`
	// <optional> the default value to use if the flag is not given
	// when combined with .configkey, default will serve as a fallback value
	// when neither the flag or the config file specified a value
	DefaultValue interface{} `yaml:"default,omitempty"`
	// shorthand flag. e.g. name=long,short=l -> --long and -l
	Short string `yaml:",omitempty"`
	// combine this flag with a config file key, and use the config value when the flag not given
	Configkey string `yaml:",omitempty"`
}

// GetConfigFileName gives the relative path to the package config file
func getConfigFileName(name ExternalPackage) string {
	return "../../../external/" + string(name) + "/cli.yaml"
}

// GetExternals returns a list of package names in /external that have a config file
func GetExternals() ([]ExternalPackage, error) {
	externals := make([]ExternalPackage, 0)

	files, err := ioutil.ReadDir("../../../external")
	if err != nil {
		return nil, err
	}

	for _, f := range files {
		if f.IsDir() {
			name := ExternalPackage(f.Name())
			configFile := getConfigFileName(name)
			if _, err := os.Stat(configFile); !os.IsNotExist(err) {
				externals = append(externals, name)
			}
		}
	}

	return externals, nil
}

// ReadConfig from a given package name
func ReadConfig(name ExternalPackage) (*ExternalConfig, error) {
	externalConfig := ExternalConfig{}
	fileName := getConfigFileName(name)
	raw, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(raw, &externalConfig)
	if err != nil {
		return nil, err
	}
	return &externalConfig, nil
}
