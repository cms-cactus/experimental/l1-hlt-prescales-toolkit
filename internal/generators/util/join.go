package util

import (
	"strings"
)

// Join executes strings.Join with no separator
func Join(input ...string) string {
	return strings.Join(input, "")
}
