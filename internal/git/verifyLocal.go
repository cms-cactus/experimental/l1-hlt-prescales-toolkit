package git

import (
	"errors"
	"l1-hlt-prescales-toolkit/internal/config"
	"l1-hlt-prescales-toolkit/internal/scriptrunner"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

// VerifyLocal checks the local git repository is valid
func VerifyLocal() error {
	log.Debug("verifying local git repo")
	localPath := viper.GetString(config.KeyGitLocal.Name)
	fullLocalPath, err := filepath.Abs(localPath)
	if err != nil {
		log.WithError(err).WithField("localPath", localPath).Error("cannot determine full prescales repo path")
		return err
	}
	f, err := os.Lstat(fullLocalPath)
	if err != nil {
		log.WithError(err).WithField("fullLocalPath", fullLocalPath).WithField("localPath", localPath).Error("prescales repo path does not exist")
		return err
	}
	if !f.IsDir() {
		err = errors.New("not a directory")
		log.WithError(err).WithField("fullLocalPath", fullLocalPath).WithField("localPath", localPath).Error("prescales repo path is not a directory")
		return err
	}

	stdout, stderr, fd3, done := scriptrunner.NewJob(fullLocalPath, "git", []string{"config", "--get", "remote.origin.url"}, nil).RunAsync()
	remote := ""
	for s := range stdout {
		if remote != "" {
			log.WithField("previous", remote).WithField("next", s).Warn("multiple remotes configured, using last one found")
		}
		remote = s
	}
	scriptrunner.Drain(stderr)
	scriptrunner.Drain(fd3)
	if err := <-done; err != nil {
		log.WithError(err).WithField("fullLocalPath", fullLocalPath).WithField("localPath", localPath).Error("can't get prescales repo remote")
		return err
	}

	configRemote := viper.GetString(config.KeyGitRemote.Name)
	if configRemote != remote {
		log.WithField("configured remote", configRemote).WithField("found remote", remote).Warn("configured remote differs from actual remote")
	}

	log.Debug("local git repo is ok")
	return nil
}
