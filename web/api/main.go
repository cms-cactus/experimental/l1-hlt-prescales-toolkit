package api

import (
	"l1-hlt-prescales-toolkit/web/api/router"
	v1 "l1-hlt-prescales-toolkit/web/api/v1"
	"net"
	"net/http"

	"github.com/skratchdot/open-golang/open"

	"github.com/go-chi/chi"
)

// StartAndListen starts the API and opens a socket on the specified port
func StartAndListen(port string) error {

	r := chi.NewRouter()

	v1.Register(r)
	router.RegisterWebUI(r)

	if port == "" {
		port = "0"
	}
	listener, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}
	address := "http://" + listener.Addr().String()
	log.WithField("address", address).Info("listening")
	open.Run(address) // opens the default browser
	return http.Serve(listener, r)
}
