package external

import (
	"github.com/go-chi/chi"
)

var externalRouter = chi.NewRouter()

type argName string
type argConf struct {
	Name         string
	ArgType      string
	DefaultValue interface{} `json:",omitempty"`
}

type externalConfig struct {
	Command     string
	Deprecated  string              `json:",omitempty"`
	Description string              `json:",omitempty"`
	Args        map[argName]argConf `json:"arguments"`
}

// Register adds this router to the given parent router
func Register(router *chi.Mux) {
	router.Mount("/external", externalRouter)
}
