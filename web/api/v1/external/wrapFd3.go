package external

import (
	"encoding/json"
	"l1-hlt-prescales-toolkit/web/api/v1/util/streamentry"
)

func wrapFd3(lines chan string) chan string {
	out := make(chan string)
	go func() {
		for line := range lines {
			wrapped := streamentry.DataCommandOutput{}
			err := json.Unmarshal([]byte(line), &wrapped)
			if err != nil {
				wrapped["raw"] = line
			}
			entry := streamentry.EntryCommandOutput(wrapped)
			bytes, err := json.Marshal(entry)
			if err != nil {
				panic(err)
			}
			out <- string(bytes)
		}
		close(out)
	}()
	return out
}
