package prescales

import (
	"github.com/go-chi/chi"
)

var psRouter = chi.NewRouter()

// Register adds this router to the given parent router
func Register(router *chi.Mux) {
	router.Mount("/prescales", psRouter)
}
