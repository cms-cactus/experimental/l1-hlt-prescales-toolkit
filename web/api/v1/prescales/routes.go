package prescales

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"

	"l1-hlt-prescales-toolkit/internal/config"

	"l1-hlt-prescales-toolkit/web/api/v1/util/errors"

	"github.com/go-chi/render"
	"github.com/spf13/viper"
)

func init() {
	psRouter.Get("/data", psGet)
	psRouter.Put("/data", psPut)
}

func psGet(w http.ResponseWriter, r *http.Request) {

	response := make(map[string]string)

	readAndAdd("columns.json", response, w, r)
	readAndAdd("l1.json", response, w, r)
	readAndAdd("hlt.json", response, w, r)
	readAndAdd("columns.schema.json", response, w, r)
	readAndAdd("l1.schema.json", response, w, r)
	readAndAdd("hlt.schema.json", response, w, r)

	render.JSON(w, r, response)
}

func psPut(w http.ResponseWriter, r *http.Request) {
	todo := make(map[string]string)
	err := json.NewDecoder(r.Body).Decode(&todo)
	if err != nil {
		log.WithError(err).Error("cannot read body")
		errors.Error(400).Send(w, r)
		return
	}
	for filename, content := range todo {
		filePath := getPath(filename)
		err := ioutil.WriteFile(filePath, []byte(content), os.ModePerm)
		if err != nil {
			log.WithError(err).Error("cannot write file")
			errors.Error(500).Send(w, r)
			return
		}
	}

	psGet(w, r)
}

func readAndAdd(filename string, m map[string]string, w http.ResponseWriter, r *http.Request) {
	str, err := read(filename)
	if err != nil {
		errors.Error(500).Send(w, r)
	}
	m[filename] = str
}

func getPath(filename string) string {
	localPath := viper.GetString(config.KeyGitLocal.Name)
	filename = filepath.Base(filename)
	return path.Join(localPath, filename)
}

func read(filename string) (string, error) {
	filePath := getPath(filename)
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.WithError(err).WithField("path", filePath).Error("cannot read file")
		return "", err
	}
	return string(bytes), nil
}
