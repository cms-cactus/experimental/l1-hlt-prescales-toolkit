import Vue from "vue";
import App from "./App.vue";
import { stores } from "./store";
import "./style";
import vuetify from "./plugins/vuetify";
import "./components/footer";

Vue.config.productionTip = false;

new Vue({
  store: stores.root,
  vuetify,
  render: h => h(App),
}).$mount("#app");
