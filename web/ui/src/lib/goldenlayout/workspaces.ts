import { ItemConfigType } from "golden-layout";
import { makePanelConfig as panel } from "./makePanelConfig";

export const workspaces: {
  [groupname: string]: { [key: string]: ItemConfigType[] };
} = {
  Generic: {
    "Manual editor": [
      {
        type: "row",
        content: [panel("Manual Editor")],
      },
    ],
    "Table editor": [
      {
        type: "row",
        content: [panel("Fancy Table")],
      },
    ],
  },
  // "example nested layout": [
  //   {
  //     type: "row",
  //     content: [
  //       panel("I am"),
  //       {
  //         type: "column",
  //         content: [panel("Table"), panel("Editor")],
  //       },
  //     ],
  //   },
  // ],
};

export const defaultWorkspace = workspaces["layout A"];
