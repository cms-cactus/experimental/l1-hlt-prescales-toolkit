import axios, {
  AxiosTransformer,
  AxiosRequestConfig,
  AxiosError,
  AxiosResponse,
} from "axios";
import { stores } from "../store";

import {
  throwAlert,
  AlertOptions,
  AlertType,
} from "../store/modules/notifications";

interface RequestConfig extends AxiosRequestConfig {
  notifyError?: "silent" | "notify" | "allow-retry";
}

const instanceConfig = {
  timeout: 20000,
  // headers: {'X-Custom-Header': 'foobar'},
  transformRequest: [
    // // eslint-disable-next-line @typescript-eslint/no-explicit-any
    // (data: any, headers: any) => {
    //   if (store.state.auth.token) {
    //     headers.Authorization = "Bearer " + store.state.auth.token;
    //   }
    //   return data;
    // },
    ...(axios.defaults.transformRequest as AxiosTransformer[]),
  ],
};

const instance = axios.create(instanceConfig);

const makeErrorReporter = (
  notifyStyle: RequestConfig["notifyError"] = "allow-retry"
) => (error: AxiosError): Promise<AxiosResponse> => {
  // eslint-disable-next-line no-console
  console.dir(error);

  if (notifyStyle == "silent" || !error) {
    return Promise.reject(error);
  }

  const response = error.response;
  let msg: string =
    (error.message || error.toString()) +
    "\n" +
    (error.config.method || "unknown method").toUpperCase() +
    " " +
    error.config.url;
  if (response) {
    let data = response.data;
    if (typeof data === "string" && data[1] === "{") {
      try {
        data = JSON.parse(data as string);
      } catch (e) {
        console.error("failed to parse JSON response", data);
      }
    }
    if (data.error) {
      msg += "\n" + data.error;
    } else if (data.message) {
      msg += "\n" + data.message;
    } else if (typeof data === "string") {
      msg += "\n" + data;
    } else {
      msg += "\n" + JSON.stringify(data);
    }
  }

  const notifyConfig: AlertOptions = {
    message: msg,
    type: AlertType.error,
    timeout: 10000,
  };
  if (notifyStyle == "allow-retry") {
    notifyConfig.actions = ["retry", "ignore"];

    const p = new Promise(resolve => {
      notifyConfig.cb = action => resolve(action);
      throwAlert(stores.root, notifyConfig);
    });
    return p.then(action => {
      if (!action || action != "retry") {
        throw error;
      } else {
        return instance
          .request(error.config)
          .catch(makeErrorReporter(notifyStyle));
      }
    });
  } else {
    throwAlert(stores.root, notifyConfig);
    throw error;
  }
};

const getConfig = (config: RequestConfig | string): RequestConfig => {
  return typeof config === "string" ? { url: config } : config;
};

const handler = (config: RequestConfig | string) => {
  const c = getConfig(config);
  return instance(c).catch(makeErrorReporter(c.notifyError));
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
(window as any).axios = handler; // for live debugging

const withMethod = (
  method: "get" | "put" | "post" | "delete" | "options",
  config: RequestConfig | string
) => {
  const c = getConfig(config);
  c.method = method;
  return handler(c);
};

handler.get = (config: RequestConfig | string) => withMethod("get", config);
handler.put = (config: RequestConfig | string) => withMethod("put", config);
handler.post = (config: RequestConfig | string) => withMethod("post", config);
handler.del = (config: RequestConfig | string) => withMethod("delete", config);
handler.options = (config: RequestConfig | string) =>
  withMethod("options", config);

export default handler;
