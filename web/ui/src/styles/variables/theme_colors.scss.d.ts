export interface GlobalScss {
  "primary-light": string;
  "primary-dark": string;
  "secondary-light": string;
  "secondary-dark": string;
  "background-light": string;
  "background-dark": string;
  accent: string;
  error: string;
  info: string;
  success: string;
  warning: string;
}

export const styles: GlobalScss;

export default styles;
