import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
} from "vuex-module-decorators";
import axios from "../../lib/axios";

export interface PSAPIData {
  "columns.json": string;
  "l1.json": string;
  "hlt.json": string;

  "columns.schema.json": string;
  "l1.schema.json": string;
  "hlt.schema.json": string;
}

@Module({ name: "prescales" })
export class PSModule extends VuexModule {
  public json: PSAPIData = {
    "columns.json": "",
    "l1.json": "",
    "hlt.json": "",

    "columns.schema.json": "",
    "l1.schema.json": "",
    "hlt.schema.json": "",
  };

  @Mutation
  _setLocalJSON(json: PSAPIData) {
    this.json = json;
  }

  @Action({ commit: "_setLocalJSON" })
  async updateRemoteJSON({
    json,
    key,
  }: {
    json: PSAPIData;
    key?: keyof PSAPIData;
  }) {
    const payload = key ? { [key]: json[key] } : json;
    await axios.put({
      url: "/api/v1/prescales/data",
      data: payload,
      notifyError: "notify",
    });
    return json;
  }

  @MutationAction({ mutate: ["json"] })
  async getRemoteJSON() {
    return {
      json: await axios.get("/api/v1/prescales/data").then(r => r.data),
    };
  }

  @Action
  async setJSONFile({ key, data }: { key: keyof PSAPIData; data: string }) {
    const newJson = Object.assign({}, this.json, { [key]: data });
    await this.updateRemoteJSON({ json: newJson, key });
  }
}
