import { Module, VuexModule, Mutation } from "vuex-module-decorators";

type catalog = { [uid: string]: string };

function getFooterElements(catalog: catalog): string[] {
  const r = Object.keys(catalog).reduce((res, key) => {
    const name = catalog[key];
    res[name] = true;
    return res;
  }, {} as { [s: string]: boolean });
  return Object.keys(r).map(name => "footer-panel-" + name);
}

@Module({ name: "footer" })
export class FooterModule extends VuexModule {
  // each footerMounter instance can request one element to be mounted in the footer
  public footerElements: string[] = [];
  private _footerElements: catalog = {};

  @Mutation
  addFooterEl({ uid, name }: { uid: number; name: string }) {
    this._footerElements[uid] = name;
    this.footerElements = getFooterElements(this._footerElements);
  }

  @Mutation
  deleteFooterEl(uid: number) {
    delete this._footerElements[uid];
    this.footerElements = getFooterElements(this._footerElements);
  }
}
