import { Store } from "vuex";
import { Module, VuexModule, Mutation } from "vuex-module-decorators";

export enum AlertType {
  info,
  warning,
  error,
  success,
}

export interface AlertOptions {
  message: string;
  type: AlertType;
  timeout?: number;
  actions?: string[];
  cb?: (actionName?: string) => void;
}

export interface Alert extends Omit<AlertOptions, "cb"> {
  count: number;
  timeout: number;
  cbChain: Array<(actionName?: string) => void>;
  actions: string[];
}

export function throwAlert(store: Store<unknown>, alert: AlertOptions) {
  store.commit("pushNotification", alert);
}

function toAlert(existing: Alert[], options: AlertOptions): Alert | null {
  const alert: Alert = Object.assign(
    {
      count: 1,
      timeout: 60000,
      actions: ["ok"],
      cbChain: [],
    },
    options
  );
  if (options.cb) {
    alert.cbChain.push(options.cb);
  }
  alert.message = alert.message.trim();

  const duplicate = existing.find(
    a => a.message === alert.message && a.type === alert.type
  );

  if (duplicate) {
    if (options.cb) {
      duplicate.cbChain.push(options.cb);
    }
    duplicate.count++;
    return null;
  }

  return alert;
}

function alertSorter(a: Alert, b: Alert) {
  if (a.type - b.type != 0) {
    return -(a.type - b.type);
  }
  if (a.count - b.count != 0) {
    return -(a.count - b.count);
  }
  if (a.message == b.message) {
    return 0;
  }
  if (a.message < b.message) {
    return -1;
  } else {
    return 1;
  }
}

@Module({ name: "notifications" })
export class NotificationsModule extends VuexModule {
  public pendingNotifications: Alert[] = [];

  @Mutation
  popNotification() {
    this.pendingNotifications.reverse();
    this.pendingNotifications.pop();
    this.pendingNotifications.reverse();
  }

  @Mutation
  pushNotification(alertOptions: AlertOptions) {
    const alert = toAlert(this.pendingNotifications, alertOptions);
    if (alert) {
      this.pendingNotifications = this.pendingNotifications
        .concat(alert)
        .sort(alertSorter);
    }
  }

  get latestNotification() {
    return this.pendingNotifications?.[0];
  }
}
