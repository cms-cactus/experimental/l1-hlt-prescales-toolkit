import Vue from "vue";
import Vuex from "vuex";

import createMutationsSharer from "vuex-shared-mutations";
import { getModule } from "vuex-module-decorators";

import { PSModule } from "./modules/prescales";
import { NotificationsModule } from "./modules/notifications";
import { FooterModule } from "./modules/footer";

Vue.use(Vuex);

const rootStore = new Vuex.Store({
  plugins: [createMutationsSharer({ predicate: () => true })],
  state: {
    message: "hello",
  },
  mutations: {
    setMessage(state, message) {
      state.message = message;
    },
    brainTransfer(state, newstate) {
      Object.assign(state, newstate);
    },
  },
  actions: {},
  modules: {
    prescales: PSModule,
    notifications: NotificationsModule,
    footer: FooterModule,
  },
});

export const stores = {
  root: rootStore,
  prescales: getModule(PSModule, rootStore),
  notifications: getModule(NotificationsModule, rootStore),
  footer: getModule(FooterModule, rootStore),
};

stores.prescales.getRemoteJSON();
