// +build !production

package ui

import "net/http"

var Assets http.FileSystem = http.Dir("web/ui/build")
